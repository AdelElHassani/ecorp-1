create table department (
	dept_name VARCHAR(50),
	dept_num INT
);
insert into department (dept_name, dept_num) values ('Services', 1);
insert into department (dept_name, dept_num) values ('Engineering', 2);
insert into department (dept_name, dept_num) values ('Product Management', 3);
insert into department (dept_name, dept_num) values ('Sales', 4);
insert into department (dept_name, dept_num) values ('Training', 5);
insert into department (dept_name, dept_num) values ('Human Resources', 6);
insert into department (dept_name, dept_num) values ('Product Management', 7);
insert into department (dept_name, dept_num) values ('Legal', 8);
insert into department (dept_name, dept_num) values ('Marketing', 9);
insert into department (dept_name, dept_num) values ('Support', 10);
insert into department (dept_name, dept_num) values ('Support', 11);
insert into department (dept_name, dept_num) values ('Legal', 12);
insert into department (dept_name, dept_num) values ('Training', 13);
insert into department (dept_name, dept_num) values ('Services', 14);
insert into department (dept_name, dept_num) values ('Marketing', 15);
insert into department (dept_name, dept_num) values ('Business Development', 16);
insert into department (dept_name, dept_num) values ('Research and Development', 17);
insert into department (dept_name, dept_num) values ('Product Management', 18);
insert into department (dept_name, dept_num) values ('Research and Development', 19);
insert into department (dept_name, dept_num) values ('Product Management', 20);
