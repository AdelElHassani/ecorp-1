# ECORP-1

## Goals

1. ~~Observe data~~
2. ~~Build model of the datebase~~
3. ~~Write a sql file to implement the database~~
4. ~~"Complete" 10 minutes pandas tutorial~~
5. Create python scripts to clean the data
6. Create a python script to insert the data into the database

## Observations

### department.sql
```
departement(dept_name, dept_num)
```
- Multiple entries have the same dept_name

### employees.csv
```
id, first_name, last_name, email, job
```
- Empty values in job column

### product_date.json
```
[ {product_id, max_date} ]
```
### products.xslx
```
name, price, available, description
```
- Multiple empty descriptions

## Model
![Class diagram](dbdiagram.drawio.png)

