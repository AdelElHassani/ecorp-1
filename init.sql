DROP DATABASE IF EXISTS ecorp;
CREATE DATABASE ecorp;

USE ecorp;

CREATE TABLE product (
    id INT
        AUTO_INCREMENT
        PRIMARY KEY
        NOT NULL,
    name VARCHAR(50)
        NOT NULL,
    description TEXT,
    price DECIMAL
        NOT NULL,
    available BOOLEAN
        NOT NULL,
    maxDate DATE
        NOT NULL
);

CREATE TABLE department (
    id INT
        AUTO_INCREMENT
        PRIMARY KEY
        NOT NULL,
    name VARCHAR(50)
        NOT NULL
        UNIQUE
);

CREATE TABLE employee (
    id INT
        AUTO_INCREMENT
        PRIMARY KEY
        NOT NULL,
    firstName VARCHAR(32)
        NOT NULL,
    lastName VARCHAR(32)
        NOT NULL,
    email VARCHAR(256)
        NOT NULL
        UNIQUE,
    departmentId INT,
    FOREIGN KEY (departmentId) REFERENCES department(id)
        ON DELETE SET NULL
);
